#include <iostream>
#include <chrono>
#include <cuda.h>
#include <cuda_runtime.h>
#include <thrust/host_vector.h>
#include <boost/program_options.hpp>

#include "tc_ddc/cuerror.cuh"
#include "tc_ddc/ddc.cuh"
#include "tc_ddc/io.h"
#include "tc_ddc/signal.h"

using namespace thrust;

template<typename ProcessorType, typename InputType, typename OutputType>
void example(ddc::ddc_t config, std::size_t runs=10)
{
  ProcessorType processor(config);
  InputType idata(config.input_size);
  OutputType odata(idata.size() * processor.up() / processor.down() * processor.nsignals());
  ddc::signal::random(idata, 0, 10);

  for(int i = 0; i < runs; i++)
  {
    auto start = std::chrono::high_resolution_clock::now();
    processor.process(idata, odata);
    CUDA_ERROR_CHECK(cudaDeviceSynchronize());
    auto stop = std::chrono::high_resolution_clock::now();
    auto ms = std::chrono::duration_cast<std::chrono::duration<double>>(stop - start);
    std::cout << "Took " << ms.count() << " s to process. Input bandwidth: " << config.input_size/ms.count() << " samples/s" << std::endl;
  }
}



int main(int argc, char** argv)
{
  ddc::ddc_t config;
  std::string ddc_type;
  std::size_t runs;
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()("fs_in,i", po::value<std::size_t>(&config.fs_in)->default_value(4000000000), "Input sample rate");
  desc.add_options()("fs_out,o", po::value<std::size_t>(&config.fs_dw)->default_value(16000000), "Output sample rate");
  desc.add_options()("f_lo,l", po::value<std::string>(&config.f_lo)->default_value("8000000"), "Local oscillator frequency");
  desc.add_options()("size,s", po::value<std::size_t>(&config.input_size)->default_value(250000000), "Number of input samples");
  desc.add_options()("type,t", po::value<std::string>(&ddc_type)->default_value("poly"), "The type of the algorithm to use ('poly', 'conv' or 'fourier'");
  desc.add_options()("runs,r", po::value<std::size_t>(&runs)->default_value(10), "Number of runs");
  po::variables_map vm;
  try
  {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if(vm.count("help"))
    {
      std::cout << "Example -- Example program to run DDC" << std::endl
      << desc << std::endl;
      return 0;
    }
    po::notify(vm);
  }
  catch(po::error& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl;
    std::cerr << desc << std::endl;
    return 1;
  }
  if(ddc_type == "poly"){
    std::cout << "Running DDC with polyphase algorithm" << std::endl;
    example<ddc::PolyphaseDownConverter<device_vector<float>, device_vector<float2>>,
      device_vector<float>, device_vector<float2>>(config, runs);
  }else if(ddc_type == "conv"){
    std::cout << "Running DDC with conventional algorithm" << std::endl;
    example<ddc::DDC<device_vector<float>, device_vector<float2>>,
      device_vector<float>, device_vector<float2>>(config, runs);
  }else if(ddc_type == "fourier"){
    std::cout << "Running DDC with fourier algorithm" << std::endl;
    example<ddc::FourierDownConverter<device_vector<float>, device_vector<float2>>,
      device_vector<float>, device_vector<float2>>(config, runs);
  }else{
    std::cout << "Algorithm " << ddc_type << " is not implemented. Doing nothing";
  }
}
