# DBBC Digital Downconverter

This repository contains implementations of three digital downconverter algorithms using C++ and CUDA. The three algorithms (conventional, polyphase and Fourier downconverters) are implemented on both the CPU and the GPU.
The algorithms produces the same outputs, but are based on different methods.

## Requirements
- cmake 3.18 or higher
- CUDA 11 or higer
- boost programm options
- (optional for testing) Google Testframework
- (optional for testing) python3 - numpy and scipy

## Install
Clone the repository
`git clone https://gitlab.mpcdf.mpg.de/nesser/tc_ddc`

Go into the root directory and create a build folder

`mkdir tc_dcc/build/ && cd tc_dcc/build/`

Prepare the installation with cmake and build the project

`cmake .. && make -j8`

Optionally, run the test to validate the builded project

`make test`

Finnaly, install the project

`make install`

### Use in Docker
Get the nvidia-cuda image from the offical docker registry

`docker pull nvidia/cuda:12.1.0-devel-ubuntu22.04` (or an other version)

Start the docker and make sure you can access the GPU

`docker run --gpus all -it nvidia/cuda:12.1.0-devel-ubuntu22.04 bash`

Install the dependencies inside the docker

`apt-get update && apt-get upgrade && apt-get install cmake libboost-program-options-dev git`

Then, follow the installation instructions mentioned above.

## Contact
Author: Niclas Esser - <nesser@mpifr-bonn.mpg.de>