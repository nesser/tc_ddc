#pragma once

#include <sstream>
#include <iostream>
#include <stdexcept>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cufft.h>


#define CUDA_ERROR_CHECK(ans) { cuda_assert_success((ans), __FILE__, __LINE__); }
/**
 * @brief Macro function for error checking on cufft calls that return cufftResult values
 */
#define CUBLAS_ERROR_CHECK(ans) { cublas_assert_success((ans), __FILE__, __LINE__); }

/**
 * @brief Macro function for error checking on cufft calls that return cufftResult values
 */
#define CUFFT_ERROR_CHECK(ans) { cufft_assert_success((ans), __FILE__, __LINE__); }

/**
 * @brief Function that raises an error on receipt of any cudaError_t
 *  value that is not cudaSuccess
 */
//inline void cuda_assert_success(cudaError_t code, const char *file, int line)
inline void cuda_assert_success(cudaError_t code, const char *file, int line)
{
    if (code != cudaSuccess)
    {
        std::stringstream error_msg;
        error_msg << "CUDA failed with error: "
              << cudaGetErrorString(code) << std::endl
              << "File: " << file << std::endl
              << "Line: " << line << std::endl;
        throw std::runtime_error(error_msg.str());
    }
}


/**
 * @brief Function that raises an error on receipt of any cufftResult
 * value that is not CUFFT_SUCCESS
 */
inline void cublas_assert_success(cublasStatus_t code, const char *file, int line)
{
    if (code != CUBLAS_STATUS_SUCCESS)
    {
        std::stringstream error_msg;
        error_msg << "CUBLAS failed with error: ";
        switch (code)
        {
        case CUBLAS_STATUS_NOT_INITIALIZED:
            error_msg <<  "CUBLAS_STATUS_NOT_INITIALIZED";
            break;

        case CUBLAS_STATUS_ALLOC_FAILED:
            error_msg <<  "CUBLAS_STATUS_ALLOC_FAILED";
            break;

        case CUBLAS_STATUS_INVALID_VALUE:
            error_msg <<  "CUBLAS_STATUS_INVALID_VALUE";
            break;

        case CUBLAS_STATUS_ARCH_MISMATCH:
            error_msg <<  "CUBLAS_STATUS_ARCH_MISMATCH";
            break;

        case CUFFT_INTERNAL_ERROR:
            error_msg <<  "CUFFT_INTERNAL_ERROR";
            break;

        case CUBLAS_STATUS_MAPPING_ERROR:
            error_msg <<  "CUBLAS_STATUS_MAPPING_ERROR";
            break;

        case CUBLAS_STATUS_EXECUTION_FAILED:
            error_msg <<  "CUBLAS_STATUS_EXECUTION_FAILED";
            break;

        case CUBLAS_STATUS_INTERNAL_ERROR:
            error_msg <<  "CUBLAS_STATUS_INTERNAL_ERROR";
            break;

        case CUBLAS_STATUS_NOT_SUPPORTED:
            error_msg <<  "CUBLAS_STATUS_NOT_SUPPORTED";
            break;

        case CUBLAS_STATUS_LICENSE_ERROR:
            error_msg <<  "CUBLAS_STATUS_LICENSE_ERROR";
            break;

        default:
            error_msg <<  "CUBLAS_UNKNOWN_ERROR";
        }
        error_msg << std::endl
              << "File: " << file << std::endl
              << "Line: " << line << std::endl;
        throw std::runtime_error(error_msg.str());
    }
}

/**
 * @brief Function that raises an error on receipt of any cufftResult
 * value that is not CUFFT_SUCCESS
 */
inline void cufft_assert_success(cufftResult code, const char *file, int line)
{
    if (code != CUFFT_SUCCESS)
    {
        std::stringstream error_msg;
        error_msg << "CUFFT failed with error: ";
        switch (code)
        {
        case CUFFT_INVALID_PLAN:
            error_msg <<  "CUFFT_INVALID_PLAN";
            break;

        case CUFFT_ALLOC_FAILED:
            error_msg <<  "CUFFT_ALLOC_FAILED";
            break;

        case CUFFT_INVALID_TYPE:
            error_msg <<  "CUFFT_INVALID_TYPE";
            break;

        case CUFFT_INVALID_VALUE:
            error_msg <<  "CUFFT_INVALID_VALUE";
            break;

        case CUFFT_INTERNAL_ERROR:
            error_msg <<  "CUFFT_INTERNAL_ERROR";
            break;

        case CUFFT_EXEC_FAILED:
            error_msg <<  "CUFFT_EXEC_FAILED";
            break;

        case CUFFT_SETUP_FAILED:
            error_msg <<  "CUFFT_SETUP_FAILED";
            break;

        case CUFFT_INVALID_SIZE:
            error_msg <<  "CUFFT_INVALID_SIZE";
            break;

        case CUFFT_UNALIGNED_DATA:
            error_msg <<  "CUFFT_UNALIGNED_DATA";
            break;

        default:
            error_msg <<  "CUFFT_UNKNOWN_ERROR";
        }
        error_msg << std::endl
              << "File: " << file << std::endl
              << "Line: " << line << std::endl;
        throw std::runtime_error(error_msg.str());
    }
}

