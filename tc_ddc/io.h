#pragma once

#include <sstream>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

namespace ddc {
namespace io {

/**
 * @brief Load data into a given vector from disk
 *
 * @tparam T Type oh the data vector
 * @param data Vector to load the data
 * @param fname file name to read
 * @param nsample number of samples to read, defaults to -1 (= whole file)
 * @return true on success
 * @return false on fail
 */
template<typename T>
bool load_vector(thrust::host_vector<T>& data, std::string fname, int nsample=-1)
{
    std::ifstream file(fname, std::ios::binary);
    if (!file.is_open()){
        std::cerr << "Failed to open file for writing" << std::endl;
        return false;
    }
    file.seekg(0, std::ios::end);
    std::streampos file_size = file.tellg();
    file.seekg(0, std::ios::beg);
    std::size_t tot_nsample = file_size / sizeof(T);
    if(nsample == -1){
        nsample = tot_nsample;
    }
    if(nsample > tot_nsample){
        std::cerr << "File " << fname << " contains only " << tot_nsample << std::endl;
        return false;
    }
    if(data.size() != nsample){
        data.resize(nsample);
    }
    std::cout << "Reading " << nsample << "/" << tot_nsample << " samples from " << fname << std::endl;
    file.read(reinterpret_cast<char*>(data.data()), nsample * sizeof(T));
    file.close();
    return true;
}

/**
 * @brief Stores a thrust::host_vector to disk as binary
 *
 * @tparam T Type of the vector
 * @param data Vector containing the data
 * @param fname file name to store
 * @return true on success
 * @return false on fail
 */
template<typename T>
bool save_vector(thrust::host_vector<T>& data, std::string fname="data.bin")
{
    std::ofstream file(fname, std::ios::binary);
    if (!file.is_open())
    {
        std::cerr << "Failed to open file for writing" << std::endl;
        return false;
    }
    std::cout << "Writing " << data.size() * sizeof(T) / 1024 << " KB to " << fname << std::endl;
    file.write(reinterpret_cast<const char*>(data.data()), data.size() * sizeof(T));
    file.close();
    return true;
}

/**
 * @brief Stores a thrust::host_vector to disk as binary
 *
 * @tparam T Type of the vector
 * @param data Vector containing the data
 * @param fname file name to store
 * @return true on success
 * @return false on fail
 */
template<typename T>
bool save_vector(thrust::device_vector<T>& data, std::string fname="data.bin")
{
    thrust::host_vector<T> idata = data;
    return save_vector<T>(idata, fname);
}

// struct real2complex_unary {
//     __host__ __device__
//     float2 operator()(float x) const {
//         float2 result;
//         result.x = x;
//         result.y = 0.0f;
//         return result;
//     }
// };

// void real2complex(
//     thrust::device_vector<float>& idata,
//     thrust::device_vector<float2>& odata)
// {
//     thrust::transform(idata.begin(), idata.end(), odata.begin(), real2complex_unary());
// }

}
}