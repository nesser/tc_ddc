#pragma once

#include <cuda.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cuComplex.h>
#include <cufft.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <cmath>

#include "tc_ddc/cuerror.cuh"
#include "tc_ddc/filter.h"
#include "tc_ddc/signal.h"
#include "tc_ddc/processing.h"

#define N_THREADS 1024
#define CUDA_PI 3.141592654f


namespace ddc {

struct ddc_t
{
  std::size_t input_size = 32000; // Input sample frequency
  std::size_t fs_in = 32000; // Input sample frequency
  std::size_t fs_dw = 4000; // Downsampling / output frequency of the sidebands
  std::string f_lo = "8000";  // Frequency of local oscillator, for multiple seperate by comma
  filter::hilbert_t hilbert;
  filter::lowpass_t lowpass;

  thrust::host_vector<float> lo_list(){
    thrust::host_vector<float> lo_vec(0);
    std::string tmp = this->f_lo + ","; std::size_t pos =0;
    while((pos = tmp.find(',')) != std::string::npos){
        lo_vec.push_back(std::stof(tmp.substr(0, pos)));
        tmp.erase(0, pos+1);
    }
    return lo_vec;
  }
  int up(){return std::lcm(fs_in, fs_dw) / fs_in;}
  int down(){return std::lcm(fs_in, fs_dw) / fs_dw;}
  int nsignals(){return this->lo_list().size();}
};

// Class description of DigitalDownconverter
template<typename InputType, typename OutputType>
class DDC
{
public:
  typedef typename InputType::value_type itype;
  typedef typename OutputType::value_type otype;
public:
  DDC(ddc_t& conf);
  ~DDC();
  void process(InputType& idata, OutputType& odata);
  std::size_t up(){return _up;}
  std::size_t down(){return _down;}
  std::size_t nsignals(){return local_osci.size();}
  InputType& lowpass(){return lowpass_fir;}
protected:
  ddc_t _conf;
  std::size_t _fs_up;
  std::size_t _up;
  std::size_t _down;
  std::size_t _lcm;
  std::size_t _max_rate;

  InputType lowpass_fir;
  InputType hilbert_fir;
  InputType local_osci;
  OutputType alpha;
  OutputType up_buffer;
  OutputType fl_buffer;
  OutputType lo_buffer;
  OutputType mx_buffer;
  OutputType rs_buffer;
};


template<typename InputType, typename OutputType>
class PolyphaseDownConverter : public DDC<InputType, OutputType>
{
public:
  typedef typename InputType::value_type itype;
  typedef typename OutputType::value_type otype;
  typedef InputType ivector_type;
  typedef OutputType ovector_type;
public:
  PolyphaseDownConverter(ddc_t& conf, std::size_t device_id=0);
  ~PolyphaseDownConverter();
  void process(InputType& input, OutputType& output);

private:
  cudaDeviceProp _cu_prop;
  std::size_t _device_id;
  thrust::device_vector<float> d_lowpass_fir;
  thrust::device_vector<float> d_hilbert_fir;
};

template<typename InputType, typename OutputType>
class FourierDownConverter : public DDC<InputType, OutputType>
{
public:
  typedef typename InputType::value_type itype;
  typedef typename OutputType::value_type otype;
public:
  FourierDownConverter(ddc_t& conf, std::size_t device_id=0);
  ~FourierDownConverter();
  void process(InputType idata, OutputType odata);

private:
  void transform_lowpass();
  cudaDeviceProp _cu_prop;
  // cublasHandle_t cublas_handle_mx;
  cublasHandle_t cublas_handle_lp;
  // cublasHandle_t cublas_handle_bp;
  cufftHandle cufft_plan_lp;
  cufftHandle cuifft_plan_lp;
  cufftHandle cufft_plan_hb;

  std::size_t _buffer_size;
  std::size_t _device_id;
  OutputType bp_buffer;
  OutputType d_lowpass_fir;
  OutputType d_hilbert_fir;

  float2 alpha = {1,0};
  float2 beta = {0,0};
  dim3 _grid;
  dim3 _block;
};



}

#include "tc_ddc/src/ddc.cu"