#pragma once

namespace ddc {


bool is_fractional(double value, double epsilon = 1e-9) {
    return std::abs(value - std::round(value)) > epsilon;
}

template<typename InputType, typename OutputType>
DDC<InputType, OutputType>::DDC(ddc_t& conf)
  : _conf(conf)
{
  // Calculate resampling values
  _up = conf.up();
  _down = conf.down();
  _fs_up = _up * conf.fs_in;
  _max_rate = (_up > _down) ? _up : _down;
  _conf.lowpass.f_cutoff = 1.0 / _max_rate;
  if(_conf.lowpass.ntaps == 0){
    _conf.lowpass.ntaps = 10 * _max_rate + 1;
  }
  std::cout << "Up: " << _up
    << " Down: " << _down
    << " LP taps:" << _conf.lowpass.ntaps << std::endl;
  local_osci = _conf.lo_list();
  lowpass_fir.resize(_conf.lowpass.ntaps);
  hilbert_fir.resize(_conf.hilbert.ntaps);
  alpha.resize(local_osci.size());
  for(std::size_t i = 0; i < local_osci.size(); i++){
    alpha[i] = {1,1};
  }
  lo_buffer.resize(_conf.input_size * local_osci.size());
  mx_buffer.resize(_conf.input_size * local_osci.size());
  up_buffer.resize(_conf.input_size * local_osci.size() * _up);
  fl_buffer.resize(_conf.input_size * local_osci.size() * _up);
  rs_buffer.resize(_conf.input_size * local_osci.size() * _up / _down);
  filter::firwin<InputType>(lowpass_fir, _conf.lowpass);
  filter::firhilbert<InputType>(hilbert_fir, _conf.hilbert);
  signal::oscillate(lo_buffer, local_osci, _conf.fs_in);
}

template<typename InputType, typename OutputType>
DDC<InputType, OutputType>::~DDC()
{
  std::cout << "Destroying DDC-object" << std::endl;
}

template<typename InputType, typename OutputType>
void DDC<InputType, OutputType>::process(InputType& idata, OutputType& odata)
{
  assert(idata.size() * _up / _down * local_osci.size() == odata.size());
  processing::mat_vec_mult(lo_buffer, idata, mx_buffer, alpha);
  processing::upsample(mx_buffer, up_buffer, _up);
  processing::convolve(up_buffer, lowpass_fir, fl_buffer, local_osci.size());
  processing::downsample(fl_buffer, rs_buffer, _down);
  processing::convolve_hilbert(rs_buffer, hilbert_fir, odata, local_osci.size());
}


/*-------------------------------*/
/*  Polyphase Filter based DDC   */
/*-------------------------------*/
template<typename InputType, typename OutputType>
PolyphaseDownConverter<InputType, OutputType>::PolyphaseDownConverter(ddc_t& conf, std::size_t device_id)
  : DDC<InputType, OutputType>(conf), _device_id(device_id)
{
    std::cout << "Constructing PolyphaseDownConverter object" << std::endl;
}

template<typename InputType, typename OutputType>
PolyphaseDownConverter<InputType, OutputType>::~PolyphaseDownConverter()
{
    std::cout << "Deconstructing PolyphaseDownConverter object" << std::endl;
}

template<typename InputType, typename OutputType>
void PolyphaseDownConverter<InputType, OutputType>::process(InputType& input, OutputType& output)
{
    assert(!is_fractional((double)input.size() * this->_up / this->_down));
    assert(input.size() * this->_up / this->_down * this->local_osci.size() == output.size());
    processing::mat_vec_mult(this->lo_buffer, input, this->mx_buffer, this->alpha);
    processing::poly_resample(this->mx_buffer, this->lowpass_fir, this->rs_buffer, this->_up, this->_down);
    processing::convolve_hilbert(this->rs_buffer, this->hilbert_fir, output, this->local_osci.size());
}


/*-------------------------------*/
/*  Fourier Transform based DDC  */
/*-------------------------------*/
template<typename InputType, typename OutputType>
FourierDownConverter<InputType, OutputType>::FourierDownConverter(ddc_t& conf, std::size_t device_id)
  : DDC<InputType, OutputType>(conf), _device_id(device_id)
{
    // assert(_buffer_size % lowpass_fir.size() == 0);
    std::cout << "Constructing FourierDownConverter object" << std::endl;
    assert(this->_up == 1);

    CUDA_ERROR_CHECK(cudaGetDeviceProperties(&_cu_prop, _device_id));
    // CUBLAS_ERROR_CHECK(cublasCreate(&cublas_handle_mx));
    CUBLAS_ERROR_CHECK(cublasCreate(&cublas_handle_lp));
    // CUBLAS_ERROR_CHECK(cublasCreate(&cublas_handle_bp));
    CUFFT_ERROR_CHECK(cufftPlan1d(
      &cufft_plan_lp, this->_conf.input_size, CUFFT_C2C, 1)
    );
    CUFFT_ERROR_CHECK(cufftPlan1d(
      &cuifft_plan_lp, this->rs_buffer.size(), CUFFT_C2C, 1)
    );

    filter::firwin<InputType>(this->lowpass_fir, this->_conf.lowpass);

    signal::oscillate(this->lo_buffer, this->local_osci, this->_conf.fs_in);
    // Allocate
    d_lowpass_fir.resize(this->lowpass_fir.size());
    this->transform_lowpass();
}


template<typename InputType, typename OutputType>
FourierDownConverter<InputType, OutputType>::~FourierDownConverter()
{
    // CUBLAS_ERROR_CHECK(cublasDestroy(cublas_handle_mx));
    CUBLAS_ERROR_CHECK(cublasDestroy(cublas_handle_lp));
    // CUBLAS_ERROR_CHECK(cublasDestroy(cublas_handle_bp));
    CUFFT_ERROR_CHECK(cufftDestroy(cufft_plan_lp));
    CUFFT_ERROR_CHECK(cufftDestroy(cuifft_plan_lp));
    // CUFFT_ERROR_CHECK(cufftDestroy(cufft_plan_hb));
}

template<typename InputType, typename OutputType>
void FourierDownConverter<InputType, OutputType>::transform_lowpass()
{
    cufftHandle tmp_plan;
    CUFFT_ERROR_CHECK(cufftPlan1d(
      &tmp_plan, this->lowpass_fir.size(), CUFFT_R2C, 1)
    );
    CUFFT_ERROR_CHECK(cufftExecR2C(
      tmp_plan,
      thrust::raw_pointer_cast(this->lowpass_fir.data()),
      thrust::raw_pointer_cast(d_lowpass_fir.data())
    ));
    CUFFT_ERROR_CHECK(cufftDestroy(tmp_plan));
}

template<typename InputType, typename OutputType>
void FourierDownConverter<InputType, OutputType>::process(InputType idata, OutputType odata)
{
  CUFFT_ERROR_CHECK(cufftExecC2C(
    cufft_plan_lp,
    thrust::raw_pointer_cast(this->mx_buffer.data()),
    thrust::raw_pointer_cast(this->mx_buffer.data()),
    CUFFT_FORWARD
  ));

  CUFFT_ERROR_CHECK(cufftExecC2C(
    cuifft_plan_lp,
    thrust::raw_pointer_cast(this->mx_buffer.data()),
    thrust::raw_pointer_cast(this->mx_buffer.data()),
    CUFFT_INVERSE
  ));
}


}