#pragma once

#include <cuda.h>

namespace ddc {
namespace processing {
namespace kernel {

/**
 * @brief
 *
 * @param idata
 * @param filter
 * @param odata
 * @param filter_size
 * @param width

 */
__global__ void convolve(
    const float* idata,
    const float* filter,
    float* odata,
    int filter_size,
    int width)
{
    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    int tidy = blockIdx.y;
    float tmp = 0;
    if(tidx < width){
        for(int j = 0; j < filter_size; j++)
        {
            int i_idx = tidy * width + tidx + j - (filter_size-1) / 2;
            if(i_idx >= tidy * width && i_idx < (tidy+1) * width){
                tmp += idata[i_idx] * filter[j];
            }
        }
        odata[tidy * width + tidx] = tmp;
    }
}

/**
 * @brief
 *
 * @param idata
 * @param filter
 * @param odata
 * @param filter_size
 * @param width

 */
__global__ void convolve(
    const float2* idata,
    const float* filter,
    float2* odata,
    int filter_size,
    int width)
{
    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    int tidy = blockIdx.y;
    float2 tmp = {.0f,.0f};
    if(tidx < width){
        for(int j = 0; j < filter_size; j++)
        {
            int i_idx = tidy * width + tidx + j - (filter_size-1) / 2;
            if(i_idx >= tidy * width && i_idx < (tidy+1) * width){
                tmp.x += idata[i_idx].x * filter[j];
                tmp.y += idata[i_idx].y * filter[j];
            }
        }
        odata[tidy * width + tidx] = tmp;
    }
}

/**
 * @brief
 *
 * @param idata
 * @param filter
 * @param odata
 * @param filter_size
 * @param width

 */
__global__ void convolve_hilbert(
    const float2* idata,
    const float* filter,
    float2* odata,
    int filter_size,
    int width)
{
    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    int tidy = blockIdx.y;
    float2 tmp = {.0f,.0f};
    if(tidx < width){
        for(int j = 0; j < filter_size; j++)
        {
            int i_idx = tidy * width + tidx - j + (filter_size-1) / 2;
            if(i_idx >= tidy * width && i_idx < (tidy+1) * width){
                tmp.x += idata[i_idx].x * filter[j];
                tmp.y += idata[i_idx].y * filter[filter_size - 1 - j];
            }
        }
        odata[tidy * width + tidx].x = tmp.x + tmp.y;
        odata[tidy * width + tidx].y = tmp.x - tmp.y;
    }
}

/**
 * @brief
 *
 * @tparam T
 * @param idata
 * @param odata
 * @param up
 * @param width

 */
template<typename T>
__global__ void upsample(
    const T* idata,
    T* odata,
    int up,
    int width)
{
    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    if(tidx < width){
        odata[tidx * up] = idata[tidx];
    }
}

/**
 * @brief
 *
 * @tparam T
 * @param idata
 * @param odata
 * @param down
 * @param width

 */
template<typename T>
__global__ void downsample(
    const T* idata,
    T* odata,
    int down,
    int width)
{
    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    if(tidx < width){
        odata[tidx] = idata[tidx * down];
    }
}


/**
 * @brief
 *
 * @param idata
 * @param filter
 * @param odata
 * @param up
 * @param down
 * @param width
 * @param ntaps
 */
__global__ void poly_resample(const float2* idata,
    const float* filter,
    float2* odata,
    int up,
    int down,
    int width,
    int ntaps)
{
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int tidy = blockIdx.y;
    int o_width = width * up / down;
    long iidx = 0;
    int half_fir = (ntaps - 1) / 2;
    extern __shared__ float s_filter[];

    for(int i = threadIdx.x; i < ntaps; i+=blockDim.x){
        s_filter[i] = filter[i];
    }
__syncthreads();

    for(long oidx = tidx; oidx < o_width; oidx+=blockDim.x*gridDim.x){
        float2 tmp = {0,0};
        int offset = (oidx * down);
        int poly_branch = ((ntaps - 1) / 2 + (up - down + up * 2) * oidx) % up;
        for(int fidx = poly_branch; fidx < ntaps; fidx+=up){
            iidx = tidy * width + (offset + fidx - half_fir) / up;
            if(iidx >= tidy * width && iidx < (tidy+1) * width){
                tmp.x += idata[iidx].x * s_filter[fidx];
                tmp.y += idata[iidx].y * s_filter[fidx];
            }
        }
        odata[tidy * o_width + oidx] = tmp;
    }
}

/**
 * @brief
 *
 * @tparam T
 * @param idata
 * @param filter
 * @param odata
 * @param up
 * @param down
 * @param width
 * @param ntaps
 */
template<typename T>
__global__ void poly_resample(const T* idata,
    const T* filter,
    T* odata,
    int up,
    int down,
    int width,
    int ntaps)
{
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    int tidy = blockIdx.y;
    int o_width = width * up / down;
    int iidx = 0;
    extern __shared__ float s_filter[];

    for(int i = threadIdx.x; i < ntaps; i+=blockDim.x){
        s_filter[i] = filter[i];
    }
__syncthreads();

    for(int oidx = tidx; oidx < o_width; oidx+=blockDim.x*gridDim.x){
        float tmp = 0;
        int poly_branch = ((ntaps - 1) / 2 + (up - down + up * 2) * oidx) % up;
        for(int fidx = poly_branch; fidx < ntaps; fidx+=up){
            iidx = tidy * width + (oidx * down + fidx - (ntaps - 1) / 2) / up;
            if(iidx >= tidy * width && iidx < (tidy+1) * width){
                tmp += idata[iidx] * s_filter[fidx];
            }
        }
        odata[tidy * o_width + oidx] = tmp;
    }

}



/**
 * @brief
 *
 * @param matrix
 * @param vector
 * @param odata
 * @param alpha
 * @param width
 * @param height

 */
__global__ void mat_vec_mult(
    const float2* matrix,
    const float* vector,
    float2* odata,
    float2* alpha,
    int width,
    int height)
{
    __shared__ float s_vec[1024];
    int glob_idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(glob_idx < width){
        s_vec[threadIdx.x] = vector[glob_idx];
        __syncthreads();
        for(int i = glob_idx; i < height*width; i+=width){
            odata[i].x = matrix[i].x * alpha[i/width].x * s_vec[threadIdx.x];
            odata[i].y = matrix[i].y * alpha[i/width].y * s_vec[threadIdx.x];
        }
    }
}


template<typename T>
__global__ void mat_vec_mult(
    T* matrix,
    const T* vector,
    int width,
    int height
)
{
    __shared__ T s_vec[1024];
    int glob_idx = threadIdx.x + blockIdx.x * blockDim.x;
    if(glob_idx < width){
        s_vec[threadIdx.x] = vector[glob_idx];
    }
    __syncthreads();
    for(int i = glob_idx; i < height*width; i+=width){
        matrix[i].x = matrix[i].x * s_vec[threadIdx.x].x - matrix[i].y * s_vec[threadIdx.x].y;
        matrix[i].y = matrix[i].x * s_vec[threadIdx.x].y + matrix[i].y * s_vec[threadIdx.x].x;
    }
}

}
}
}