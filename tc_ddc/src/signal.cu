#pragma once

#include <cuda.h>

namespace ddc {
namespace signal {
namespace kernel {

__global__ void chirp(
    float* data,
    float fs,
    float f0,
    float f1,
    float gain,
    float t1,
    int width)
{
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    float duration = width / fs;
    float beta = (f1 - f0) / duration, phase, t;
    if(tid < width)
    {
        t = tid / fs;
        phase = 2 * M_PI * (f0 * t + 0.5 * beta * t * t);
        data[tid] = gain * cosf(phase);
    }
}

__global__ void oscillate(
    float2* data,
    float* lo,
    float fs,
    int width,
    int height)
{
    int tidx = threadIdx.x + blockIdx.x * blockDim.x;
    int tidy = blockIdx.y;
    if(tidx < width && tidy < height)
    {
        double phase = -2 * M_PI * lo[tidy] * tidx / fs;
        data[tidy * width + tidx].x = (float)cos(phase);
        data[tidy * width + tidx].y = (float)sin(phase);
    }
}


}
}
}