#pragma once
#include <cuda.h>
#include <random>
#include <cmath>
#include <numeric>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

namespace ddc {
namespace signal {
namespace kernel {

__global__ void chirp(float* data, float fs, float f0, float f1, float gain, float t1, int width);
__global__ void oscillate(float2* data, float* lo, float fs, int width, int height);

}


__host__ __device__ float sinc(float x)
{
  return (x == 0) ? 1 : sin(M_PI * x) / (M_PI * x);
}


void chirp(
    thrust::device_vector<float>& data,
    float fs,
    float f0,
    float f1,
    float gain=1,
    float t1=-1)
{
    std::size_t width = data.size();
    if(t1 < 0){t1 = width / fs;}
    kernel::chirp<<<width / 1024 + 1, 1024>>>(
        thrust::raw_pointer_cast(data.data()),
        fs, f0, f1, gain, t1, width
    );
}

void chirp(
    thrust::host_vector<float>& data,
    float fs,
    float f0,
    float f1,
    float gain=1,
    float t1=-1)
{
    float duration = data.size() / fs;
    float beta = (f1 - f0) / duration;
    float phase, t;
    if(t1 < 0){t1=duration;}
    for(std::size_t i = 0; i < data.size(); i++)
    {
        t = i / fs;
        phase = 2 * M_PI * (f0 * t + 0.5 * beta * t * t);
        data[i] = gain * cos(phase);
    }
}
void chirp(
    thrust::host_vector<float2>& data,
    float fs,
    float f0,
    float f1,
    float gain=1,
    float t1=-1)
{
    float duration = data.size() / fs;
    float beta = (f1 - f0) / duration;
    float phase, t;
    if(t1 < 0){t1=duration;}
    for(std::size_t i = 0; i < data.size(); i++)
    {
        t = i / fs;
        phase = 2 * M_PI * (f0 * t + 0.5 * beta * t * t);
        data[i].x = gain * cos(phase);
        data[i].y = gain * sin(phase);
    }
}


template<typename T>
void random(
    thrust::host_vector<T>& data,
    int mean,
    int sigma)
{
  std::default_random_engine engine;
  std::normal_distribution<double> dist(mean, sigma);
  for (std::size_t i = 0; i < data.size(); i++)
  {
    if constexpr (std::is_same<float2, T>::value){
      data[i].x = decltype(T::x)(dist(engine));
      data[i].y = decltype(T::y)(dist(engine));
    }else if (std::is_integral<T>::value){
      data[i] = (T)(dist(engine) + 1);
    }else{
      data[i] = (T)(dist(engine));
    }
  }
}
template<typename T>
void random(
    thrust::device_vector<T>& data,
    int mean,
    int sigma)
{
    thrust::host_vector<T> tmp(data.size());
    signal::random(tmp, mean, sigma);
    data = tmp;
}


void oscillate(
    thrust::device_vector<float2>& data,
    thrust::device_vector<float>& f_lo,
    std::size_t fs)
{
    assert(data.size() % f_lo.size() == 0);
    std::size_t length = data.size() / f_lo.size();
    dim3 grid(length / 1024 + 1, f_lo.size(), 1);
    kernel::oscillate<<<grid, 1024>>>(
        thrust::raw_pointer_cast(data.data()),
        thrust::raw_pointer_cast(f_lo.data()),
        fs, length, f_lo.size()
    );
}

void oscillate(
    thrust::host_vector<float2>& data,
    thrust::host_vector<float>& f_lo,
    std::size_t fs)
{
    assert(data.size() % f_lo.size() == 0);
    std::size_t length = data.size() / f_lo.size();
    for(std::size_t i = 0; i < f_lo.size(); i++)
    {
        for(std::size_t ii = 0; ii < length; ii++)
        {
            double phase = -2 * M_PI * f_lo[i] * ii / fs;
            data[i * length + ii].x = cos(phase);
            data[i * length + ii].y = sin(phase);
        }
    }
}

}
}
#include "src/signal.cu"