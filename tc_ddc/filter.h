#pragma once

#include <cuda.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <numeric>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include "tc_ddc/signal.h"

namespace ddc {
namespace filter {
namespace kernel {

__global__ void firhilbert(float* data, int length, float a, float w1, float w2);
template<typename T>__global__ void firwin(T* data, T* window, std::size_t length, float f_cutoff);
template<typename T>__global__ void window_hamming(T* idata, int length);
template<typename T>__global__ void window_hann(T* idata, int length);
template<typename T>__global__ void window_bartlett(T* idata, int length);
template<typename T>__global__ void window_blackman(T* idata, int length);
// __global__ void mix_poly_resample(const float* idata, const float2* oscillation, float2* odata, std::size_t up, std::size_t down, float time_offset);

}

struct hilbert_t
{
    std::size_t ntaps = 301;
    float a = 0.05f;
    float w1 = 0.05f;
    float w2 = 0.45f;
};

struct lowpass_t
{
    std::size_t ntaps = 0;
    std::string window = "hamming";
    double f_cutoff;
};


void firhilbert(
    thrust::device_vector<float>& data,
    float a=0.05,
    float w1=0.05,
    float w2=0.45)
{
    std::size_t length = data.size();
    kernel::firhilbert<<<1024 / length + 1, 1024, 0>>>(
        thrust::raw_pointer_cast(data.data()),
        length, a, w1, w2
    );

}
void firhilbert(
    thrust::host_vector<float>& data,
    float a=0.05,
    float w1=0.05,
    float w2=0.45)
{
    float t;
    std::size_t length = data.size();
    for(int i = 0; i < length; i++)
    {
        // Calculate t
        t = 2 * M_PI * (i - (int)(length - 1) / 2);
        if(t == 0){
            data[i] = sqrt(2) * (w2 - w1);
        }else if(t == M_PI/2*a){
            data[i] = a * (sin(M_PI/4 * ( (a+2*w2) / a)) - sin( M_PI/4 *( (a+2*w1) / a )));
        }else if(t == -M_PI/2*a){
            data[i] = a * (sin(M_PI/4 * ( (a-2*w1) / a)) - sin( M_PI/4 *( (a-2*w2) / a )));
        }else{
            data[i] = 2 * M_PI * M_PI * cos(a * t)
                / (t * (4 * a * a * t * t - M_PI * M_PI))
                * (sin(w1 * t + M_PI / 4)
                - sin(w2 * t + M_PI / 4));
        }
    }
}

template<typename VectorType>
void firhilbert(VectorType& data, hilbert_t conf)
{
    firhilbert(data, conf.a, conf.w1, conf.w2);
}

template<typename T>
void window(
    thrust::device_vector<T>& data,
    std::string window_name)
{
    std::size_t length = data.size();
    if(window_name == "hamming"){
        kernel::window_hamming<<<length / 1024 + 1, 1024>>>(
            thrust::raw_pointer_cast(data.data()), length
        );
    }
    else if(window_name == "hann"){
        kernel::window_hann<<<length / 1024 + 1, 1024>>>(
            thrust::raw_pointer_cast(data.data()), length
        );
    }
    else if(window_name == "hamming"){
        kernel::window_bartlett<<<length / 1024 + 1, 1024>>>(
            thrust::raw_pointer_cast(data.data()), length
        );
    }
    else if(window_name == "hamming"){
        kernel::window_blackman<<<length / 1024 + 1, 1024>>>(
            thrust::raw_pointer_cast(data.data()), length
        );
    }
    else{
        std::cout << "Window function " << window_name
            << " not implemented, using rectangle" << std::endl;
        thrust::fill(data.begin(), data.end(), 1);
    }
}

template<typename T>
void window(
    thrust::host_vector<T>& data,
    std::string window_name)
{
    std::size_t length = data.size();
    if(window_name == "hamming"){
        for(std::size_t i = 0; i < length; i++){
            data[i] = 0.54 - 0.46 * cos(2 * M_PI * i / (length - 1));
        }
    }else{
        std::cout << "Window function " << window_name
            << " not implemented, using rectangle" << std::endl;
        thrust::fill(data.begin(), data.end(), 1);
    }
}



template<typename T>
void firwin(
    thrust::device_vector<T>& data,
    float f_cutoff=0.25,
    std::string window="hamming")
{
    std::size_t length = data.size();
    thrust::device_vector<T> window_function(length);
    filter::window(window_function, window);
    kernel::firwin<<<1024 / length + 1, 1024, 0>>>(
        thrust::raw_pointer_cast(data.data()),
        thrust::raw_pointer_cast(window_function.data()),
        length, f_cutoff
    );
}

template<typename T>
void firwin(
    thrust::host_vector<T>& data,
    float f_cutoff=0.25,
    std::string window="hamming")
{
    assert(f_cutoff >= 0 && f_cutoff <= 1);
    std::size_t length = data.size();
    thrust::host_vector<T>window_func(length);
    filter::window(window_func, window);
    for(std::size_t i = 0; i < length; i++)
    {
        data[i] = f_cutoff
            * signal::sinc(((T)i-length/2)*f_cutoff)
            * window_func[i];
    }
}

template<typename VectorType>
void firwin(
    VectorType& data,
    lowpass_t conf)
{
    firwin(data, conf.f_cutoff, conf.window);
}

}
}

#include "src/filter.cu"