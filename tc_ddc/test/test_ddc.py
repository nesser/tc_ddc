import argparse
from argparse import RawTextHelpFormatter
import numpy as np
import scipy.signal as ss
from math import sin, cos, pi, sqrt

def hilbert_fir(ntaps=301, a=0.05, w1=0.05, w2=0.45, dtype="float32", reverse=False):
    """
    Description: Calculates filter coeffiecents for an Hilbert bandpass filter
    """
    output=[]

    for i in range(ntaps):
        t = 2 * pi * (i - (ntaps-1)/2)
        if t == 0:
            o = sqrt(2)*(w2-w1)
        elif t == pi/2*a:
            o = a*(sin(pi/4*((a+2*w2)/a))-sin(pi/4*((a+2*w1)/a)))
        elif t == -pi/2*a:
            o = a*(sin(pi/4*((a-2*w1)/a))-sin(pi/4*((a-2*w2)/a)))
        else:
            o = 2*(pi**2)*cos(a*t)/(t*(4*(a**2)*(t**2)-pi**2))*(sin(w1*t+pi/4)-sin(w2*t+pi/4))
        output.append(o)
    if reverse:
        output.reverse()
    return np.asarray(output, dtype=dtype)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('--oscillations', '-lo', action='store', default="2e6", dest="oscillations",
                        help="The local oscillator freuencies, seperated by ','")
    parser.add_argument('--sample_clock','-s', action='store', default=4.096e9, dest="sample_clock",
                        help="The initial sample frequency in samples/s")
    parser.add_argument('--output_clock','-c', action='store', default=128e6, dest="output_clock",
                        help="The output sample clock (2 times the bandwidth)")
    parser.add_argument('--dtype','-d', action='store', default='float32', dest="dtype",
                        help="Data type of the input stream")
    parser.add_argument('--nsamples','-n', action='store', default=524288, dest="nsamples",
                        help="Number of samples per input buffer")
    parser.add_argument('--ifile','-i', action='store', default="tmp_input.dat", dest="ifile",
                        help="Number of samples per input buffer")
    parser.add_argument('--ofile','-o', action='store', default="tmp_output.dat", dest="ofile",
                        help="Number of samples per input buffer")
    parser.add_argument('--lpfile','-l', action='store', default="tmp_lowpass.dat", dest="lfile",
                        help="Number of samples per input buffer")

    # Parse user input
    ifile = str(parser.parse_args().ifile)
    ofile = str(parser.parse_args().ofile)
    lfile = str(parser.parse_args().lfile)
    osci = [float(lo) for lo in parser.parse_args().oscillations.split(",")]
    fs_in = int(float(parser.parse_args().sample_clock))
    fs_out = int(float(parser.parse_args().output_clock))
    dtype = np.dtype(parser.parse_args().dtype)
    nsample = int(parser.parse_args().nsamples)

    idata = np.fromfile(ifile, dtype=dtype)
    lowpass = np.fromfile(lfile, dtype=dtype)

    lcm = np.lcm(fs_in, fs_out)
    up = int(lcm // fs_in)
    dw = int(lcm // fs_out)
    h_coeff_r = hilbert_fir()
    h_coeff_i = hilbert_fir(reverse=True)

    t_vector = np.arange(nsample) / fs_in
    # Create LO signals
    os = np.zeros((len(osci), len(t_vector)), dtype=np.complex64)
    for i, lo in enumerate(osci):
        os[i] = np.exp(-1j * 2 * np.pi * lo * t_vector)
    # Process the data
    mx = idata * os
    rs = ss.resample_poly(mx, up, dw, window=lowpass, axis=1) / up
    re = np.zeros(rs.shape, dtype=dtype)
    im = np.zeros(rs.shape, dtype=dtype)
    for i in range(len(rs)):
        re[i] = np.convolve(np.real(rs[i]), h_coeff_r, mode="same")
        im[i] = np.convolve(np.imag(rs[i]), h_coeff_i, mode="same")
    ls = re + im
    us = re - im
    odata = np.asarray([ls, us], dtype=dtype).transpose(1,2,0).flatten()
    odata.tofile(ofile)
    rs.tofile("rs_cpu.dat")
    mx.tofile("mx_cpu.dat")
    os.tofile("os_cpu.dat")

