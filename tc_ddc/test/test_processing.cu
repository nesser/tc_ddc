
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <gtest/gtest.h>
#include <chrono>

#include "tc_ddc/cuerror.cuh"
#include "tc_ddc/io.h"
#include "tc_ddc/signal.h"
#include "tc_ddc/filter.h"
#include "tc_ddc/processing.h"

namespace ddc{
namespace processing{
namespace test{

template<typename T>
void compare(thrust::host_vector<T> a, thrust::host_vector<T> b, float abs=0.01, float rel=0.05)
{
    ASSERT_EQ(a.size(), b.size());
    if constexpr (std::is_same<float2, T>::value){
        for(std::size_t i = 0; i < a.size(); i++){
            ASSERT_NEAR(a[i].x, b[i].x, abs + std::fabs(a[i].x) * rel) << "Position: " << i;
            ASSERT_NEAR(a[i].y, b[i].y, abs + std::fabs(a[i].x) * rel) << "Position: " << i;
        }
    }else{
        for(std::size_t i = 0; i < a.size(); i++){
            ASSERT_NEAR(a[i], b[i], abs + std::fabs(a[i]) * rel) << "Position: " << i;
        }
    }
}


/*-----------------------------*/
/*  Generalized test functions */
/*-----------------------------*/

/**
 * @brief Tests if the upsample function on the GPU produces the same output as the CPU
 *
 * @tparam T Type of the in- and output
 * @param length Length of the signal
 * @param up The up factor
 * @param mean Mean of the input data
 * @param sigma Devitation of the input data
 */
template<typename T>
void test_upsample(std::size_t length, std::size_t up, float mean = 0, float sigma=10){
    thrust::host_vector<T> h_idata(length);
    thrust::host_vector<T> h_odata(length*up);
    signal::random(h_idata, mean, sigma);
    thrust::device_vector<T> idata = h_idata;
    thrust::device_vector<T> odata(length*up);
    processing::upsample(h_idata, h_odata, up);
    processing::upsample(idata, odata, up);
    thrust::host_vector<T> gpu = odata;
    compare(gpu, h_odata);
}
// Upsample tests
TEST(upsample, test_upsample_real_against_reference){
    test_upsample<float>(2049, 16);
}
TEST(upsample, test_upsample_cplx_against_reference){
    test_upsample<float2>(123456, 16);
}


/**
 * @brief Tests if the downsample function on the GPU produces the same output as the CPU
 *
 * @tparam T Type of the in- and output
 * @param length Length of the signal
 * @param down The down factor
 * @param mean Mean of the input data
 * @param sigma Devitation of the input data
 */
template<typename T>
void test_downsample(std::size_t length, std::size_t down, float mean = 0, float sigma=10){
    thrust::host_vector<T> h_idata(length);
    thrust::host_vector<T> h_odata(length / down);
    signal::random(h_idata, mean, sigma);
    thrust::device_vector<T> idata = h_idata;
    thrust::device_vector<T> odata(length / down);
    processing::downsample(h_idata, h_odata, down);
    processing::downsample(idata, odata, down);
    thrust::host_vector<T> gpu = odata;
    compare(gpu, h_odata);
}
// Downsample tests
TEST(downsample, test_downsample_real_against_reference){
    test_downsample<float>(16000, 16);
}
TEST(downsample, test_downsample_cplx_against_reference){
    test_downsample<float2>(32000, 16);
}

/**
 * @brief Tests if the convolve function on the GPU produces the same output as the CPU
 *
 * @tparam T Type of the in- and output
 * @param signal_length Length of the signal
 * @param filter_length Length of the FIR filter
 * @param n_signals Number of signals to convolve
 * @param mean Mean of the input data
 * @param sigma Devitation of the input data
 */
template<typename T>
void test_convolve(std::size_t signal_length, std::size_t filter_length, std::size_t n_signals=1, float mean = 0, float sigma=10){
    thrust::host_vector<T> h_idata(signal_length * n_signals);
    thrust::host_vector<T> h_odata(signal_length * n_signals);
    thrust::host_vector<float> h_filter(filter_length);
    signal::random( h_idata, mean, sigma );
    filter::firwin( h_filter );
    thrust::device_vector<T> idata = h_idata;
    thrust::device_vector<T> odata(signal_length * n_signals);
    thrust::device_vector<float> filt = h_filter;

    processing::convolve(idata, filt, odata, n_signals);
    processing::convolve(h_idata, h_filter, h_odata, n_signals);

    thrust::host_vector<T> gpu = odata;
    compare(gpu, h_odata);
}
// Convolve tests
TEST(convolve, test_convolve_real_against_reference){
    test_convolve<float>(123456, 1234);
}
TEST(convolve, test_convolve_cplx_against_reference){
    test_convolve<float2>(123456, 1234);
}
TEST(convolve, test_convolve_real_multiple_signals_against_reference){
    test_convolve<float>(12345, 1234, 3);
}
TEST(convolve, test_convolve_cplx_multiple_signals_against_reference){
    test_convolve<float2>(12345, 1234, 3);
}

/**
 * @brief Tests if the mat_vec_mult function on the GPU produces the same output as the CPU
 *
 * @tparam T Type of the in- and output
 * @param length Length of the matrix
 * @param height Height of the matrix
 * @param mean Mean of the input data
 * @param sigma Devitation of the input data
 */
template<typename T>
void test_mat_vec_mult(std::size_t length, std::size_t height, float mean = 0, float sigma=10)
{
    // Allocate
    thrust::host_vector<float> h_vector(length);
    thrust::host_vector<T> h_matrix(length * height);
    thrust::host_vector<T> h_odata(length * height);
    thrust::host_vector<T> alpha(height, {1,1});
    signal::random(h_vector, mean, sigma);
    signal::random(h_matrix, mean, sigma);
    thrust::device_vector<float> vector = h_vector;
    thrust::device_vector<T> d_alpha = alpha;
    thrust::device_vector<T> matrix = h_matrix;
    thrust::device_vector<T> odata(length * height);

    processing::mat_vec_mult( matrix, vector, odata, d_alpha );
    processing::mat_vec_mult( h_matrix, h_vector, h_odata, alpha );

    thrust::host_vector<float2> gpu = odata;
    compare(gpu, h_odata);
}
// Elementwise matrix-vector multiplication tests
// TEST(mat_vec_mult, test_mat_vec_mult_real_against_reference){
//     test_mat_vec_mult<float>(12345, 12);
// }
TEST(mat_vec_mult, test_mat_vec_mult_cplx_against_reference){
    test_mat_vec_mult<float2>(12345, 12);
}


/**
 * @brief Testing configuration (only used for ResampleTester)
 */
struct resample_t{
    std::size_t fs_in;
    std::size_t fs_out;
    std::size_t nsamples;
    std::size_t fir_mult;
};
/**
 * @brief Class for parameterized testing of the resampler implementations
 */
class ResampleTester : public ::testing::TestWithParam<resample_t>
{
protected:
  void SetUp(){};
  void TearDown(){};
public:
    ResampleTester() : ::testing::TestWithParam<resample_t>(),
        _conf(GetParam()),
        _fs_in(_conf.fs_in),
        _fs_out(_conf.fs_out),
        _nsamples(_conf.nsamples),
        _fir_mult(_conf.fir_mult)
    {
        std::size_t lcm = std::lcm(_fs_in, _fs_out);
        _up = lcm / _fs_in;
        _down = lcm / _fs_out;
        std::size_t max_rate = (_up > _down) ? _up : _down;
        lp.f_cutoff = 1.0 / max_rate;
        lp.ntaps = max_rate * _fir_mult;
        if(lp.ntaps % 2 == 0){lp.ntaps++;}
        std::cout << "up: " << _up << " down: " << _down << " Ftaps: " << lp.ntaps << std::endl;
    }

    /**
     * @brief Test the polyphase resampler against the conventional resampler on CPU
     *
     * @tparam VectorType Type of in- and output
     * @tparam FilterType Type of the FIR filter
     */
    template<typename VectorType, typename FilterType>
    void test_poly_resample_against_resample()
    {
        // Code duplication due to GTest limitation - type and value parameterized tests
        thrust::host_vector<VectorType> idata(_nsamples);
        thrust::host_vector<FilterType> filter(lp.ntaps);
        thrust::host_vector<VectorType> odata_gold(_nsamples * _up / _down);
        thrust::host_vector<VectorType> odata_test(_nsamples * _up / _down);
        signal::random<VectorType>(idata, 0, 10);
        filter::firwin( filter , lp);
        // Run the polyphase resampler on the CPU
        processing::poly_resample(idata, filter, odata_test, _up, _down);
        // Run the naive resmapling implementation
        processing::resample(idata, filter, odata_gold, _up, _down);
        compare(odata_test, odata_gold, 0.1);
    }

    /**
     * @brief Test the polyphase resampler GPU against the conventional resampler on CPU
     *
     * @tparam VectorType Type of in- and output
     * @tparam FilterType Type of the FIR filter
     */
    template<typename VectorType, typename FilterType>
    void test_poly_resample_against_resample_gpu()
    {
        // Code duplication due to GTest limitation - type and value parameterized tests
        thrust::host_vector<VectorType> idata(_nsamples);
        thrust::host_vector<FilterType> filter(lp.ntaps);
        thrust::host_vector<VectorType> odata_gold(_nsamples * _up / _down);
        thrust::host_vector<VectorType> odata_test(_nsamples * _up / _down);
        signal::random<VectorType>(idata, 0, 10);
        filter::firwin( filter , lp);
        // Allocate GPU buffers
        thrust::device_vector<VectorType> d_idata = idata;
        thrust::device_vector<FilterType> d_filter = filter;
        thrust::device_vector<VectorType> d_odata = odata_gold;
        // Run the polyphase resampler on the CPU
        processing::poly_resample(d_idata, d_filter, d_odata, _up, _down);
        // Run the naive resmapling implementation
        processing::resample(idata, filter, odata_gold, _up, _down);
        odata_test = d_odata;
        compare(odata_test, odata_gold);
    }

    /**
     * @brief Test the conventional resampler GPU against the conventional resampler on CPU
     *
     * @tparam VectorType Type of in- and output
     * @tparam FilterType Type of the FIR filter
     */
    template<typename VectorType, typename FilterType>
    void test_resample_gpu()
    {
        // Code duplication due to GTest limitation - type and value parameterized tests
        thrust::host_vector<VectorType> idata(_nsamples);
        thrust::host_vector<FilterType> filter(lp.ntaps);
        thrust::host_vector<VectorType> odata_gold(_nsamples * _up / _down);
        thrust::host_vector<VectorType> odata_test(_nsamples * _up / _down);
        signal::random<VectorType>(idata, 0, 10);
        filter::firwin( filter , lp);
        // Allocate GPU buffers
        thrust::device_vector<VectorType> d_idata = idata;
        thrust::device_vector<FilterType> d_filter = filter;
        thrust::device_vector<VectorType> d_odata = odata_gold;
        // Run the naive resampler on the GPU
        processing::resample(d_idata, d_filter, d_odata, _up, _down);
        // Run the naive resampler on the CPU
        processing::resample(idata, filter, odata_gold, _up, _down);
        odata_test = d_odata;
        compare(odata_test, odata_gold);
    }

    /**
     * @brief Test the polyphase resampler GPU against the polyphase resampler on CPU
     *
     * @tparam VectorType Type of in- and output
     * @tparam FilterType Type of the FIR filter
     */
    template<typename VectorType, typename FilterType>
    void test_poly_resample_gpu()
    {
        // Code duplication due to GTest limitation - type and value parameterized tests
        thrust::host_vector<VectorType> idata(_nsamples);
        thrust::host_vector<FilterType> filter(lp.ntaps);
        thrust::host_vector<VectorType> odata_gold(_nsamples * _up / _down);
        thrust::host_vector<VectorType> odata_test(_nsamples * _up / _down);
        signal::random<VectorType>(idata, 0, 10);
        filter::firwin( filter , lp);
        // Allocate GPU buffers
        thrust::device_vector<VectorType> d_idata = idata;
        thrust::device_vector<FilterType> d_filter = filter;
        thrust::device_vector<VectorType> d_odata = odata_gold;
        // Run reference implementation on CPU
        processing::poly_resample(idata, filter, odata_gold, _up, _down);
        // Run GPU implementation
        processing::poly_resample(d_idata, d_filter, d_odata, _up, _down);
        // Transfer output from device to host
        odata_test = d_odata;
        compare(odata_test, odata_gold);
    }

private:
    resample_t _conf;
    filter::lowpass_t lp;
    std::size_t _fs_in;
    std::size_t _fs_out;
    std::size_t _fir_mult;
    std::size_t _nsamples;
    std::size_t _up;
    std::size_t _down;
};


/*---------------------------------------- */
/*  Parameterized tests for ResampleTester */
/*---------------------------------------- */
/**
 * @brief Construct a new TEST_P using the conventional resampler on the GPU
 *        Output products are compared against the same algorithm on the CPU
 */
TEST_P(ResampleTester, TestConventionalResamplerDeviceFloat2){
  std::cout << std::endl
    << "-------------------------------------------------------------------" << std::endl
    << " Testing conventional resampler against convential on CPU (float2)" << std::endl
    << "-------------------------------------------------------------------" << std::endl;
  test_resample_gpu<float2, float>();
}

/**
 * @brief Construct a new TEST_P using the conventional resampler on the GPU
 *        Output products are compared against the same algorithm on the CPU
 */
TEST_P(ResampleTester, TestConventionalResamplerDeviceFloat){
  std::cout << std::endl
    << "------------------------------------------------------------------" << std::endl
    << " Testing conventional resampler against convential on CPU (float) " << std::endl
    << "------------------------------------------------------------------" << std::endl;
  test_resample_gpu<float, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the CPU
 *        Output products are compared against the convential algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolyResamplerHostFloat2){
  std::cout << std::endl
    << "-------------------------------------------------------------------" << std::endl
    << " Testing polyphase resampler against convential on CPU (float2)" << std::endl
    << "-------------------------------------------------------------------" << std::endl;
  test_poly_resample_against_resample<float2, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the CPU
 *        Output products are compared against the convential algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolyResamplerHostFloat){
  std::cout << std::endl
    << "---------------------------------------------------------------" << std::endl
    << " Testing polyphase resampler against convential on CPU (float) " << std::endl
    << "---------------------------------------------------------------" << std::endl;
  test_poly_resample_against_resample<float, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the GPU
 *        Output products are compared against the convential algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolylResamplerDeviceFloat2){
  std::cout << std::endl
    << "-----------------------------------------" << std::endl
    << " Testing polyphase resampler GPU (float2)" << std::endl
    << "-----------------------------------------" << std::endl;
  test_poly_resample_against_resample_gpu<float2, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the GPU
 *        Output products are compared against the convential algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolylResamplerDeviceFloat){
  std::cout << std::endl
    << "-----------------------------------------" << std::endl
    << " Testing polyphase resampler GPU (float) " << std::endl
    << "-----------------------------------------" << std::endl;
  test_poly_resample_against_resample_gpu<float, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the GPU
 *        Output products are compared against the polyphase algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolyphaseResamplerDeviceAgainstConventionalFloat2){
  std::cout << std::endl
    << "-----------------------------------------------------------------" << std::endl
    << " Testing polyphase resampler GPU against polyphase resampler CPU " << std::endl
    << "-----------------------------------------------------------------" << std::endl;
  test_poly_resample_gpu<float2, float>();
}

/**
 * @brief Construct a new TEST_P using the polyphase resampler on the GPU
 *        Output products are compared against the polyphase algorithm on the CPU
 */
TEST_P(ResampleTester, TestPolyphaseResamplerDevice){
  std::cout << std::endl
    << "-----------------------------------------------------------------" << std::endl
    << " Testing polyphase resampler GPU against polyphase resampler CPU " << std::endl
    << "-----------------------------------------------------------------" << std::endl;
  test_poly_resample_gpu<float, float>();
}

INSTANTIATE_TEST_SUITE_P(ResampleTesterInstantiation, ResampleTester, ::testing::Values(
    resample_t{32000, 500, 32000, 10},
    resample_t{750, 300, 750, 1},
    resample_t{7, 5, 35, 2},
    resample_t{4, 3, 24, 7},
    resample_t{3, 2, 24, 4},
    resample_t{3200, 75, 32000, 5},
    resample_t{550, 100, 22000, 10},
    resample_t{500, 100, 20000, 10},
    resample_t{650, 100, 13000, 10},
    resample_t{40, 33, 13200, 10},
    resample_t{3200, 750, 48000, 10},
    resample_t{750, 300, 1500, 10},
    resample_t{4000000000, 16000000, 200000, 20},
    resample_t{32000, 500, 320000, 20}
));

}
}
}