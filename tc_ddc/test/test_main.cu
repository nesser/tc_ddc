#include <gtest/gtest.h>

#include "tc_ddc/test/test_signal.cu"
#include "tc_ddc/test/test_filter.cu"
#include "tc_ddc/test/test_processing.cu"
#include "tc_ddc/test/test_io.cu"
#include "tc_ddc/test/test_ddc.cu"

int main(int argc, char **argv) {
 ::testing::InitGoogleTest(&argc, argv);

 return RUN_ALL_TESTS();
}
