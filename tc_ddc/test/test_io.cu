
#include <filesystem>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <gtest/gtest.h>

#include "tc_ddc/signal.h"
#include "tc_ddc/io.h"

namespace ddc{
namespace io{
namespace test{

TEST(store_load, store_load_float)
{
    thrust::host_vector<float> i_vector(2000);
    thrust::host_vector<float> o_vector(2000);
    signal::random(i_vector, 10, 20);
    ASSERT_TRUE(io::save_vector(i_vector, "tmp.dat"));
    ASSERT_TRUE(io::load_vector(o_vector, "tmp.dat"));
    std::filesystem::remove("tmp.dat");
    for(std::size_t i = 0; i < i_vector.size(); i++){
        ASSERT_EQ(i_vector[i], o_vector[i]);
    }
}

TEST(store_load, store_load_float2)
{
    thrust::host_vector<float2> i_vector(2000);
    thrust::host_vector<float2> o_vector(2000);
    signal::random(i_vector, 10, 20);
    ASSERT_TRUE(io::save_vector(i_vector, "tmp.dat"));
    ASSERT_TRUE(io::load_vector(o_vector, "tmp.dat"));
    std::filesystem::remove("tmp.dat");
    for(std::size_t i = 0; i < i_vector.size(); i++){
        ASSERT_EQ(i_vector[i].x, o_vector[i].x);
        ASSERT_EQ(i_vector[i].y, o_vector[i].y);
    }
}

TEST(store_load, store_load_int)
{
    thrust::host_vector<int> i_vector(2000);
    thrust::host_vector<int> o_vector(2000);
    signal::random(i_vector, 10, 20);
    ASSERT_TRUE(io::save_vector(i_vector, "tmp.dat"));
    ASSERT_TRUE(io::load_vector(o_vector, "tmp.dat"));
    std::filesystem::remove("tmp.dat");
    for(std::size_t i = 0; i < i_vector.size(); i++){
        ASSERT_EQ(i_vector[i], o_vector[i]);
    }
}

TEST(store_load, store_load_int_half_vector)
{
    thrust::host_vector<int> i_vector(2000);
    thrust::host_vector<int> o_vector(1000);
    signal::random(i_vector, 10, 20);
    ASSERT_TRUE(io::save_vector(i_vector, "tmp.dat"));
    ASSERT_TRUE(io::load_vector(o_vector, "tmp.dat", 1000));
    std::filesystem::remove("tmp.dat");
    for(std::size_t i = 0; i < o_vector.size(); i++){
        ASSERT_EQ(i_vector[i], o_vector[i]);
    }
}

TEST(store_load_int_half_vector, store_load_int)
{
    thrust::host_vector<int> i_vector(2000);
    thrust::host_vector<int> o_vector(1000);
    signal::random(i_vector, 10, 20);
    ASSERT_TRUE(io::save_vector(i_vector, "tmp.dat"));
    ASSERT_FALSE(io::load_vector(o_vector, "tmp.dat", 2001));
    std::filesystem::remove("tmp.dat");
}

}
}
}
