#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <gtest/gtest.h>
#include <chrono>
#include <filesystem>
#include <cstdlib>

#include "tc_ddc/io.h"
#include "tc_ddc/ddc.cuh"

namespace ddc {
namespace test {

/**
 * @brief A class for testing the implemented downconverters against
 *        a python implementation. The python implementation is considered
 *        to be the golden sandard. Inherits from ::testing::TestWithParam
 */
class DDCTester : public ::testing::TestWithParam<ddc_t>
{
protected:
  void SetUp(){};
  void TearDown(){};

public:
/**
 * @brief Construct a new DDCTester object
 *
 */
    DDCTester() : ::testing::TestWithParam<ddc_t>(),
        _conf(GetParam())
    {
        h_input.resize(_conf.input_size);
        gold_output.resize(h_input.size()
            * _conf.up()
            / _conf.down()
            * _conf.nsignals()
        );
        test_output.resize(h_input.size()
            * _conf.up()
            / _conf.down()
            * _conf.nsignals()
        );
        // signal::random(h_input, 0, 10);
        signal::chirp(h_input, _conf.fs_in, 1, _conf.fs_in / 2);
    }

    /**
     * @brief Destroy the DDCTester object
     *
     */
    ~DDCTester(){}


    /**
     * @brief Run the python implementation by exchanging the input, filter
     *        and output data.
     */
    void reference()
    {
        std::string script_path = __FILE__;
        script_path.replace(script_path.end()-2, script_path.end(), "py");
        std::filesystem::path current_path = std::filesystem::current_path();
        std::filesystem::path ifile = current_path / "tmp_input.dat";
        std::filesystem::path ofile = current_path / "tmp_output.dat";
        std::filesystem::path lfile = current_path / "tmp_lowpass.dat";

        io::save_vector(h_input, "tmp_input.dat");
        if (!std::filesystem::exists(script_path) || !std::filesystem::exists(ifile) || !std::filesystem::exists(lfile)) {
            std::cerr << "Python script or input file not found." << std::endl;
        }
        std::string cmd = "python " + script_path
            + " -i " + ifile.string()
            + " -o " + ofile.string()
            + " -l " + lfile.string()
            + " -s " + std::to_string(_conf.fs_in)
            + " -c " + std::to_string(_conf.fs_dw)
            + " -lo " + _conf.f_lo
            + " -n " + std::to_string(_conf.input_size);
        std::cout << "Python command: " << cmd << std::endl;
        int res = std::system(cmd.c_str());

        if(res == 0 && std::filesystem::exists(ofile)){
            io::load_vector(gold_output, ofile.string());
            std::filesystem::remove(ifile);
            // std::filesystem::remove(ofile);
            std::filesystem::remove(lfile);
        }
    }

    /**
     * @brief Run the DDC tests with different ProcessorTypes
     *
     * @tparam ProcessorType defines the used processor
     *         (e.g. DDC, PolyphaseDownConverter, FourierDownConverter)
     * @tparam InputType The input type, either thrust::host_vector<T> or thrust::device_vector<T>
     * @tparam OutputType The output type, either thrust::host_vector<T> or thrust::device_vector<T>
     */
    template<typename ProcessorType, typename InputType, typename OutputType>
    void run_test()
    {
        std::filesystem::path current_path = std::filesystem::current_path();
        std::filesystem::path lfile = current_path / "tmp_lowpass.dat";
        ProcessorType processor(_conf);
        InputType input = h_input;
        OutputType output = test_output;
        processor.process(input, output);
        test_output = output;
        io::save_vector(processor.lowpass(), lfile.string());
        this->reference();
        for(std::size_t i = 0; i < gold_output.size(); i++){
            ASSERT_NEAR(test_output[i].x, gold_output[i].x, 0.05 + std::fabs(gold_output[i].x) * 0.05) << " Position " << i;
            ASSERT_NEAR(test_output[i].y, gold_output[i].y, 0.05 + std::fabs(gold_output[i].x) * 0.05) << " Position " << i;
        }
    }

private:
    ddc_t _conf;
    thrust::host_vector<float> h_input;
    thrust::host_vector<float2> gold_output;
    thrust::host_vector<float2> test_output;
};


// Define the different processor types
using DDCHostType = ddc::DDC<thrust::host_vector<float>, thrust::host_vector<float2>>;
using DDCDeviceType = ddc::DDC<thrust::device_vector<float>, thrust::device_vector<float2>>;
using PolyHostType = ddc::PolyphaseDownConverter<thrust::host_vector<float>, thrust::host_vector<float2>>;
using PolyDeviceType = ddc::PolyphaseDownConverter<thrust::device_vector<float>, thrust::device_vector<float2>>;
using FourierHostType = ddc::FourierDownConverter<thrust::host_vector<float>, thrust::host_vector<float2>>;
using FourierDeviceType = ddc::FourierDownConverter<thrust::device_vector<float>, thrust::device_vector<float2>>;


/**
 * @brief Construct a new TEST_P using the DDC on the CPU
 *        Output products are compared against a python implementation
 */
TEST_P(DDCTester, TestDDCHost){
  std::cout << std::endl
    << "-------------------------------" << std::endl
    << " Testing convential DDC on CPU " << std::endl
    << "-------------------------------" << std::endl;
  run_test<DDCHostType, thrust::host_vector<float>, thrust::host_vector<float2>>();
}
/**
 * @brief Construct a new TEST_P using the DDC on the GPU
 *        Output products are compared against a python implementation
 */
TEST_P(DDCTester, TestDDCDevice){
  std::cout << std::endl
    << "-------------------------------" << std::endl
    << " Testing convential DDC on GPU " << std::endl
    << "-------------------------------" << std::endl;
  run_test<DDCDeviceType, thrust::device_vector<float>, thrust::device_vector<float2>>();
}
/**
 * @brief Construct a new TEST_P using the PolyphaseDownConverter on the CPU
 *        Output products are compared against a python implementation
 */
TEST_P(DDCTester, TestPolyphaseDownConverterHost){
  std::cout << std::endl
    << "---------------------------------------" << std::endl
    << " Testing PolyphaseDownConverter on CPU " << std::endl
    << "---------------------------------------" << std::endl;
  run_test<PolyHostType, thrust::host_vector<float>, thrust::host_vector<float2>>();
}
/**
 * @brief Construct a new TEST_P using the PolyphaseDownConverter on the GPU
 *        Output products are compared against a python implementation
 */
TEST_P(DDCTester, TestPolyphaseDownConverterDevice){
  std::cout << std::endl
    << "---------------------------------------" << std::endl
    << " Testing PolyphaseDownConverter on GPU " << std::endl
    << "---------------------------------------" << std::endl;
  run_test<PolyDeviceType, thrust::device_vector<float>, thrust::device_vector<float2>>();
}
/**
 * @brief Construct a new INSTANTIATE_TEST_SUITE_P object
 *        Parameterize the defined TEST_P objects
 */
INSTANTIATE_TEST_SUITE_P(DDCTesterInstantiation, DDCTester, ::testing::Values(
  ddc_t{60000, 750, 500, "300", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}},
  ddc_t{48000, 32000, 750, "7777", {301, 0.05, 0.05, 0.45}, {0, "rect", 0}},
  ddc_t{48000, 3200, 750, "666", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}},
  ddc_t{350000, 700, 500, "8000", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}},
  ddc_t{132000, 40, 33, "15", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}},
  ddc_t{32000, 32000, 4000, "8000", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}},
  ddc_t{11000000, 55000, 10000, "4300", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}},
  ddc_t{9600000, 250, 150, "75", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}},
  ddc_t{77000, 11, 7, "1234,245,123,222", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}},
  ddc_t{132000, 12, 11, "999", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}},
  ddc_t{96000, 32000, 2000, "0,2000,4000,6000", {301, 0.05, 0.05, 0.45}, {0, "hamming", 0}}
));

}
}