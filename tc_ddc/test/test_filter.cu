#include <gtest/gtest.h>
#include <iostream>
#include <chrono>
#include "tc_ddc/filter.h"

namespace ddc{
namespace filter{
namespace test{


TEST(firhilbert, test_firhilbert_against_reference){
    std::size_t taps = 433;
    thrust::device_vector<float> gpu(taps);
    thrust::host_vector<float> cpu(taps);
    filter::firhilbert(gpu);
    filter::firhilbert(cpu);
    EXPECT_EQ(cpu.size(), gpu.size());
    for(std::size_t i = 0; i < cpu.size(); i++){
        EXPECT_NEAR(gpu[i], cpu[i], 0.001);
    }
}

TEST(window_hamming, test_window_hamming_against_reference){
    int width = 32000;
    // Allocate
    thrust::device_vector<float> d_gpu(width);
    thrust::host_vector<float> cpu(width);
    // Process
    window( d_gpu, "hamming" );
    window( cpu, "hamming" );
    // Copy device memory to host
    thrust::host_vector<float> gpu = d_gpu;
    // Compare
    EXPECT_EQ(cpu.size(), gpu.size());
    for(std::size_t i = 0; i < cpu.size(); i++){
        EXPECT_NEAR(gpu[i], cpu[i], 0.001);
    }
}

TEST(window_rect, test_window_rect_against_reference){
    int width = 32000;
    // Allocate
    thrust::device_vector<float> d_gpu(width);
    thrust::host_vector<float> cpu(width);
    // Process
    filter::window( d_gpu, "rect" );
    filter::window( cpu, "rect" );
    // Copy device memory to host
    thrust::host_vector<float> gpu = d_gpu;
    // Compare
    EXPECT_EQ(cpu.size(), gpu.size());
    for(std::size_t i = 0; i < cpu.size(); i++){
        EXPECT_NEAR(gpu[i], cpu[i], 0.001);
    }
}

TEST(firwin, test_firwin_against_reference){
    std::size_t taps = 433;
    thrust::device_vector<float> gpu(taps);
    thrust::host_vector<float> cpu(taps);
    filter::firwin(gpu);
    filter::firwin(cpu);
    EXPECT_EQ(cpu.size(), gpu.size());
    for(std::size_t i = 0; i < cpu.size(); i++){
        EXPECT_NEAR(gpu[i], cpu[i], 0.001);
    }
}





}
}
}