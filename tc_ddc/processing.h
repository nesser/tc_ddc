#pragma once

#include <cuda.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <numeric>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

namespace ddc {
namespace processing {

// GPU kernel defintion
namespace kernel {

__global__ void convolve(const float* idata, const float* lfilter, float* odata, int filter_size, int width);
__global__ void convolve(const float2* idata, const float* lfilter, float2* odata, int filter_size, int width);
__global__ void convolve_hilbert(const float2* idata, const float* lfilter, float2* odata, int filter_size, int width);
template<typename T>__global__ void upsample(const T* idata, T* odata, int up, int width);
template<typename T>__global__ void downsample(const T* idata, T* odata, int down, int width);
template<typename T>__global__ void poly_resample(const T* idata, const T* lfilter, T* odata, int up, int down, int width, int ntaps);
template<typename T>__global__ void poly_resample_eff(const T* idata, const T* lfilter, T* odata, int up, int down, int width, int ntaps);
__global__ void poly_resample(const float2* idata, const float* lfilter, float2* odata, int up, int down, int width, int ntaps);
__global__ void mat_vec_mult(const float2* matrix, const float* vector, float2* odata, float2* alpha, int width, int height);
template<typename T> __global__ void mat_vec_mult(T* matrix, const T* vector, int width, int height);

}

// Constants
const int N_SM = 512;
const int N_THREADS = 1024;
const int WARP_SIZE = 32;
const int N_WARPS = 32;

/*------------------------*/
/*  Convolution functions */
/*------------------------*/
/**
 * @brief Convolves input with filter of the same type using the GPU
 *
 * @param input The first signal - 1 or 2 dimensional
 * @param filter The second signal - 1 dimension
 * @param output The convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve(
    const thrust::device_vector<float>& input,
    const thrust::device_vector<float>& filter,
    thrust::device_vector<float>& output,
    std::size_t n_signals=1)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = filter.size();
    assert(signal_length >= filter_length);
    assert(signal_length * n_signals == output.size());
    dim3 grid(signal_length / 1024 + 1, n_signals, 1);
    kernel::convolve<<<grid, 1024, 0>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(filter.data()),
        thrust::raw_pointer_cast(output.data()),
        filter_length, signal_length
    );
}


/**
 * @brief Convolves input with filter of the same type using the CPU
 *
 * @param input The first signal - 1 or 2 dimensional
 * @param filter The second signal - 1 dimension
 * @param output The convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve(
    const thrust::host_vector<float>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float>& output,
    std::size_t n_signals=1)
{
    int signal_length = input.size() / n_signals;
    int filter_length = lfilter.size();

    assert(signal_length >= filter_length);
    assert(signal_length * n_signals == output.size());
    for (int n = 0; n < n_signals; n++)
    {
        for (int oidx = n * signal_length; oidx < (n+1) * signal_length; oidx++)
        {
            output[oidx] = 0;
            for(int fidx = 0; fidx < lfilter.size(); fidx++)
            {
                int iidx = oidx + fidx - (lfilter.size()-1) / 2;
                if(iidx >= n * signal_length && iidx < (n+1) * signal_length){
                    output[oidx] += input[iidx] * lfilter[fidx];
                }
            }
        }
    }
}

/**
 * @brief Complex-real convolution using the GPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param filter The second signal - 1 dimension of real type
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& filter,
    thrust::device_vector<float2>& output,
    std::size_t n_signals=1)
{
    std::size_t signal_length = input.size() / n_signals;
    std::size_t filter_length = filter.size();
    assert(signal_length >= filter_length);
    assert(signal_length * n_signals == output.size());
    dim3 grid(signal_length / 1024 + 1, n_signals, 1);
    kernel::convolve<<<grid, 1024, 0>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(filter.data()),
        thrust::raw_pointer_cast(output.data()),
        filter.size(), signal_length
    );
}


/**
 * @brief Complex-real convolution using the CPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param filter The second signal - 1 dimension of real type
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float2>& output,
    std::size_t n_signals=1)
{
    int signal_length = input.size() / n_signals;
    int filter_length = lfilter.size();

    assert(signal_length >= filter_length);
    assert(signal_length * n_signals == output.size());
    for (int n = 0; n < n_signals; n++)
    {
        for (int oidx = n * signal_length; oidx < (n+1) * signal_length; oidx++)
        {
            output[oidx] = {0,0};
            for(int fidx = 0; fidx < lfilter.size(); fidx++)
            {
                int iidx = oidx + fidx - (lfilter.size()-1) / 2;
                if(iidx >= n * signal_length && iidx < (n+1) * signal_length){
                    output[oidx].x += input[iidx].x * lfilter[fidx];
                    output[oidx].y += input[iidx].y * lfilter[fidx];
                }
            }
        }
    }
}


/**
 * @brief Complex-real convolution using the GPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param filter The second signal - 1 dimension of real type
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve_hilbert(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& filter,
    thrust::device_vector<float2>& output,
    std::size_t n_signals=1)
{
    assert((input.size() / n_signals) % 1 == 0);
    std::size_t signal_length = input.size() / n_signals;
    dim3 grid(signal_length / 1024 + 1, n_signals, 1);
    kernel::convolve_hilbert<<<grid, 1024, 0>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(filter.data()),
        thrust::raw_pointer_cast(output.data()),
        filter.size(), signal_length
    );
}

/**
 * @brief Complex-real convolution using the CPU
 *
 * @param input The first signal - 1 or 2 dimensional of complex type
 * @param filter The second signal - 1 dimension of real type
 * @param output The complex convolved output
 * @param n_signals If input is 2 dimensional specify second axis. Defaults to 1
 *
 * ToDo: make in template
 */
void convolve_hilbert(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float2>& output,
    std::size_t n_signals=1)
{
    int signal_length = input.size() / n_signals;
    int filter_length = lfilter.size();

    assert(signal_length >= filter_length);
    assert(input.size() == output.size());

    for(int n = 0; n < n_signals; n++){
        for (int i = 0; i < signal_length; i++){
            float2 tmp = {.0,.0};
            for (int j = 0; j < filter_length; j++){
                int iidx = n * signal_length + i - j + (filter_length - 1) / 2;
                if (iidx >= n * signal_length && iidx < (n+1) * signal_length){
                    tmp.x += input[n * signal_length + iidx].x * lfilter[j];
                    tmp.y += input[n * signal_length + iidx].y * lfilter[filter_length - 1 - j];
                }
            }
            output[n * signal_length + i].x = tmp.x + tmp.y;
            output[n * signal_length + i].y = tmp.x - tmp.y;
        }
    }
}


/*--------------------------*/
/*  Up/Downsample functions */
/*--------------------------*/
/**
 * @brief Upsamples a signal by zero filling on the GPU
 *
 * @param input The input signal to upsample
 * @param output The upsampled output signal
 * @param up The upsample factor
 */
template<typename T>
void upsample(
    const thrust::device_vector<T>& input,
    thrust::device_vector<T>& output,
    int up)
{
    assert(input.size() * up == output.size());
    kernel::upsample<<<input.size() / 1024 + 1, 1024, 0>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(output.data()),
        up, input.size()
    );
}


/**
 * @brief Upsamples a signal by zero filling on the CPU
 *
 * @param input The input signal to upsample
 * @param output The upsampled output signal
 * @param up The upsample factor
 */
template<typename T>
void upsample(
    const thrust::host_vector<T>& input,
    thrust::host_vector<T>& output,
    int up)
{
    assert(input.size() * up == output.size());
    for(int i = 0; i < input.size(); i++){
        output[i*up] = input[i];
    }
}


/**
 * @brief Downsamples a signal by skipping on the GPU
 *
 * @param input The input signal to downsample
 * @param output The downsampled output signal
 * @param up The downsample factor
 */
template<typename T>
void downsample(
    const thrust::device_vector<T>& input,
    thrust::device_vector<T>& output,
    int down)
{
    assert(input.size() / down == output.size());
    kernel::downsample<<<output.size() / 1024 + 1, 1024, 0>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(output.data()),
        down, output.size()
    );
}


/**
 * @brief Downsamples a signal by skipping on the GPU
 *
 * @param input The input signal to downsample
 * @param output The downsampled output signal
 * @param up The downsample factor
 */
template<typename T>
void downsample(
    const thrust::host_vector<T>& input,
    thrust::host_vector<T>& output,
    int down)
{
    assert(input.size() / down == output.size());
    for(int i = 0; i < output.size(); i++){
        output[i] = input[i*down];
    }
}


/*---------------------*/
/*  Resample functions */
/*---------------------*/
/**
 * @brief Naive rational resampling
 *
 * @param input The complex input signal
 * @param lfilter The real FIR filter
 * @param output The complex output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 *
 * @detail This is a very naive and slow resampling approach. It will allocate temporary buffers
 */
template<typename VectorType, typename FilterType>
void resample(
    const VectorType& input,
    const FilterType& lfilter,
    VectorType& output,
    int up,
    int down
)
{
    assert(output.size() == input.size() * up / down);
    VectorType upsampled(up*input.size());
    VectorType filtered(upsampled.size());
    processing::upsample(input, upsampled, up);
    processing::convolve(upsampled, lfilter, filtered);
    processing::downsample(filtered, output, down);
}


/**
 * @brief Rational resampling using a polyphase filter on the CPU - faster and efficent
 *
 * @param input The complex input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The complex output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
void poly_resample(
    const thrust::host_vector<float2>& input,
    const thrust::host_vector<float>& lfilter,
    thrust::host_vector<float2>& output,
    int up,
    int down,
    int n_signal = 1)
{
    assert(output.size() == input.size() * up / down);
    int signal_length = input.size() / n_signal;
    int iidx = 0;
    for(int n = 0; n < n_signal; n++){
        int oidx_offset = n * signal_length * up / down;
        int iidx_offset = n * signal_length;
        for (int oidx = 0; oidx < output.size(); oidx++){
            int poly_branch = ((lfilter.size() - 1) / 2 + (up - down + up * 2) * oidx) % up;
            for(int fidx = poly_branch; fidx < lfilter.size(); fidx += up){
                iidx = (oidx * down + fidx - (lfilter.size() - 1) / 2) / up + iidx_offset;
                if (iidx >= n * signal_length && iidx < (n+1) * signal_length){
                    output[oidx + oidx_offset].x += input[iidx].x * lfilter[fidx];
                    output[oidx + oidx_offset].y += input[iidx].y * lfilter[fidx];
                }
            }
        }
    }
}


/**
 * @brief Rational resampling using a polyphase filter on the CPU - faster and efficent
 *
 * @param input The real input signal - 1 or 2 dimensional
 * @param filter The real FIR filter
 * @param output The real output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
template<typename T>
void poly_resample(
    const thrust::host_vector<T>& input,
    const thrust::host_vector<T>& lfilter,
    thrust::host_vector<T>& output,
    int up,
    int down,
    int n_signal = 1)
{
    assert(output.size() == input.size() * up / down);
    int signal_length = input.size() / n_signal;
    int iidx = 0;
    for(int n = 0; n < n_signal; n++){
        int oidx_offset = n * signal_length * up / down;
        int iidx_offset = n * signal_length;
        for (int oidx = 0; oidx < output.size(); oidx++){
            int poly_branch = ((lfilter.size() - 1) / 2 + (up - down + up * 2) * oidx) % up;
            for(int fidx = poly_branch; fidx < lfilter.size(); fidx += up){
                iidx = (oidx * down + fidx - (lfilter.size() - 1) / 2) / up + iidx_offset;
                if (iidx >= n * signal_length && iidx < (n+1) * signal_length){
                    output[oidx + oidx_offset] += input[iidx] * lfilter[fidx];
                }
            }
        }
    }
}

/**
 * @brief Rational resampling using a polyphase filter on the GPU - faster and efficent
 *
 * @param input The complex input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The complex output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
void poly_resample(
    const thrust::device_vector<float2>& input,
    const thrust::device_vector<float>& lfilter,
    thrust::device_vector<float2>& output,
    std::size_t up,
    std::size_t down,
    int n_signal = 1)
{
    dim3 grid;
    if(output.size() > (N_SM / n_signal * N_THREADS)){
        grid.x = N_SM / n_signal;
    }else{
        grid.x = output.size() / N_THREADS + 1;
    }
    grid.y = n_signal;
    int s_memory = lfilter.size() * sizeof(float);
    kernel::poly_resample<<<grid, N_THREADS, s_memory>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(lfilter.data()),
        thrust::raw_pointer_cast(output.data()),
        up, down, input.size() / n_signal, lfilter.size()
    );
}

/**
 * @brief Rational resampling using a polyphase filter on the GPU - faster and efficent
 *
 * @param input The real input signal - 1 or 2 dimensional
 * @param lfilter The real FIR filter
 * @param output The real output signal
 * @param up The upsampling factor
 * @param down The downsampling factor
 * @param n_signal Number of signals to resample
 */
template<typename T>
void poly_resample(
    const thrust::device_vector<T>& input,
    const thrust::device_vector<T>& lfilter,
    thrust::device_vector<T>& output,
    std::size_t up,
    std::size_t down,
    int n_signal = 1)
{
    dim3 grid;
    grid.x = (N_SM * N_THREADS < input.size()) ? N_SM / n_signal : input.size() / N_THREADS + 1;
    grid.y = n_signal;
    int s_memory = lfilter.size() * sizeof(T);
    kernel::poly_resample<<<grid, N_THREADS, s_memory>>>(
        thrust::raw_pointer_cast(input.data()),
        thrust::raw_pointer_cast(lfilter.data()),
        thrust::raw_pointer_cast(output.data()),
        up, down, input.size() / n_signal, lfilter.size()
    );
}


/**
 * @brief elementwise multiplication of matrix and vector
 *
 * @param matrix the matrix
 * @param vector
 * @param output
 * @param alpha
 * @param stream
 */
void mat_vec_mult(
    thrust::device_vector<float2>& matrix,
    thrust::device_vector<float>& vector,
    thrust::device_vector<float2>& output,
    thrust::device_vector<float2>& alpha,
    cudaStream_t stream=NULL)
{
    assert(matrix.size() % vector.size() == 0);
    assert(output.size() == matrix.size());
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    dim3 grid(cols / 1024 + 1, rows, 1);
    kernel::mat_vec_mult<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(matrix.data()),
        thrust::raw_pointer_cast(vector.data()),
        thrust::raw_pointer_cast(output.data()),
        thrust::raw_pointer_cast(alpha.data()), cols, rows
    );
}

/**
 * @brief
 *
 * @tparam T
 * @param matrix
 * @param vector
 * @param stream
 */
template<typename T>
void mat_vec_mult(
    thrust::device_vector<T>& matrix,
    thrust::device_vector<T>& vector,
    cudaStream_t stream=NULL)
{
    assert(matrix.size() % vector.size() == 0);
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    dim3 grid(cols / 1024 + 1, rows, 1);
    kernel::mat_vec_mult<<<grid, 1024, 0, stream>>>(
        thrust::raw_pointer_cast(matrix.data()),
        thrust::raw_pointer_cast(vector.data()),
        cols, rows
    );
}

/**
 * @brief
 *
 * @param matrix
 * @param vector
 * @param output
 * @param alpha
 */
void mat_vec_mult(
    thrust::host_vector<float2>& matrix,
    thrust::host_vector<float>& vector,
    thrust::host_vector<float2>& output,
    thrust::host_vector<float2>& alpha)
{
    assert(matrix.size() % vector.size() == 0);
    assert(output.size() == matrix.size());
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    assert(rows == alpha.size());
    for(std::size_t i = 0; i < rows; i++)
    {
        for(std::size_t ii = 0; ii < cols; ii++)
        {
            output[i * cols + ii].x = vector[ii] * alpha[i].x * matrix[i * cols + ii].x;
            output[i * cols + ii].y = vector[ii] * alpha[i].y * matrix[i * cols + ii].y;
        }
    }
}

/**
 * @brief
 *
 * @param matrix
 * @param vector
 */
void mat_vec_mult(
    thrust::host_vector<float2>& matrix,
    thrust::host_vector<float>& vector)
{
    assert(matrix.size() % vector.size() == 0);
    std::size_t rows = matrix.size() / vector.size();
    std::size_t cols = vector.size();
    for(std::size_t i = 0; i < rows; i++)
    {
        for(std::size_t ii = 0; ii < cols; ii++)
        {
            matrix[i * cols + ii].x = vector[ii] * matrix[i * cols + ii].x;
            matrix[i * cols + ii].y = vector[ii] * matrix[i * cols + ii].y;
        }
    }
}

}
}
#include "src/processing.cu"