option(ENABLE_CUDA "Enable CUDA algorithms. Requires CUDA toolkit to be installed" ON)

find_package(CUDA REQUIRED)
include_directories(${CUDA_TOOLKIT_INCLUDE})
# add sdk samples useful headerfiles like cuda_helpers.h
# if(CUDA_SMP_INC)
# include_directories(${CUDA_SMP_INC})
# endif(CUDA_SMP_INC)

set(CUDA_HOST_COMPILER ${CMAKE_CXX_COMPILER})
set(CUDA_PROPAGATE_HOST_FLAGS OFF)
set(CMAKE_CUDA_STANDARD 17)
set(CMAKE_CUDA_ARCHITECTURES "75;80")
add_definitions(-DENABLE_CUDA)

list(APPEND CUDA_NVCC_FLAGS -DENABLE_CUDA -std=c++${CMAKE_CXX_STANDARD} -Wno-deprecated-gpu-targets)
list(APPEND CUDA_NVCC_FLAGS_DEBUG --debug --generate-line-info  -Xcompiler "-Wextra")  # Do not use Xcompiler -Werror here as it prevents some kernels from execution
list(APPEND CUDA_NVCC_FLAGS_PROFILE --generate-line-info)
string(TOUPPER "${CMAKE_BUILD_TYPE}" uppercase_CMAKE_BUILD_TYPE)
if(NOT uppercase_CMAKE_BUILD_TYPE MATCHES "DEBUG")
  message("Enabling device specific compilation as not in DEBUG mode")
  list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_75,code=sm_75) # RTX2080, CUDA10
  list(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_80,code=sm_80) # RTX3090
endif(NOT uppercase_CMAKE_BUILD_TYPE MATCHES "DEBUG")


list(APPEND CUDA_NVCC_FLAGS -O3 -use_fast_math -restrict)

set(CMAKE_CXX_FLAGS "-DENABLE_CUDA ${CMAKE_CXX_FLAGS}")

get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
add_definitions(-DUSE_NVTX)
find_library(CUDA_NVTOOLSEXT_LIB
  NAMES nvToolsExt
  HINTS ${CUDA_TOOLKIT_ROOT_DIR}/lib64)
  list(APPEND CUDA_DEPENDENCY_LIBRARIES ${CUDA_NVTOOLSEXT_LIB})
