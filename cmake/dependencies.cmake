include(cuda)
include(compiler_settings)

find_package(Boost COMPONENTS program_options REQUIRED)

set(DEPENDENCY_LIBRARIES
    ${Boost_LIBRARIES}
    ${CUDA_CUDART_LIBRARY}
)
